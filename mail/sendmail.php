<?php


	$jsondata = new stdClass();
	$jsondata = json_decode(file_get_contents("php://input"));

	$jsonresponse = array();
	$jsonresponse['success'] = false;
	$jsonresponse['message'] = "";

	function sendResponse($jsonresponse){
		header('Content-type: application/json; charset=utf-8');
    	echo json_encode($jsonresponse);
    	exit();
	}
 
	if(empty($jsondata->nombre) ||
	   empty($jsondata->correo) || 
	   empty($jsondata->telefono) || 
	   empty($jsondata->mensaje) || 
	   !filter_var($jsondata->correo, FILTER_VALIDATE_EMAIL))
	{
		$jsonresponse['success'] = false;
		$jsonresponse['message'] = "Debe rellenar todos los campos!";
		sendResponse($jsonresponse);
	}

	$name = strip_tags($jsondata->nombre);

	$email_address = filter_var($jsondata->correo, FILTER_VALIDATE_EMAIL);

	if ($email_address === FALSE)
	{
		$jsonresponse['success'] = false;
		$jsonresponse['message'] = "El correo es invalido!";
		sendResponse($jsonresponse);
	}

	

	$phone = strip_tags($jsondata->telefono);
	$message = strip_tags($jsondata->mensaje, '<br><br/><p>');  
	$to = "gourmetdeshanghai@outlook.com";
	$email_subject = "Correo enviado desde ComidaChina.com.ar:  ".$name; 
	$email_body = "<html>
						<body>
							<center>
								<img src='https://www.comidachina.com.ar/img/logo-small.png' width='100'>
								<h3>&nbsp;</h3>
								Se ha recibido un nuevo correo desde el Formulario de Contacto
							</center><br />
							<b>Nombre:</b> ".$name."<br />
							<b>Correo:</b> ".$email_address."<br />
							<b>Telefono:</b> ".$phone."<br />
							<b>Mensaje:</b>
							<br />".$message."
						</body>								
								</html>";
	$headers = "From: noreply@comidachina.com.ar\r\n"; 
	$headers .= "Reply-To: ".strip_tags($email_address)."\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=utf-8\r\n";
	

	$success = mail($to,$email_subject,$email_body,$headers);

	try{
		if ($success){
			$jsonresponse['success'] = true;
			$jsonresponse['message'] = "Su correo ha sido enviado, Sr(a). ". $name."<br>Gracias por su contacto";
			sendResponse($jsonresponse);
		}else{
			$jsonresponse['success'] = false;
			$jsonresponse['message'] = "Lo sentimos, ha ocurrido un error en nuestra plataforma al intentar enviar su correo. Por favor intente nuevamente";
			sendResponse($jsonresponse);
		}
	}catch( Exception $error){
		
			$jsonresponse['success'] = false;
			$jsonresponse['message'] = $error.getMessage();
			sendResponse($jsonresponse);
		
	}


?>

