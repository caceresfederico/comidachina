$(fetch('data/menu.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(oData) {
    let oItems = '';
    oData.forEach((oCategory, index) => {
        let oItemOpen = `<div class="menu-item">
                                <h3 class="text-fit text-primary">${oCategory.title}</h3>
                                <div class="row">`;
        let oItemContent = '';
        oCategory.items.forEach((oItem, index) => {
                oItemContent += `
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="list-group">
                        <div class="list-group-item">
                            <span class="badge bg-primary">$${oItem.Price}</span>
                            <h5 class="list-group-item-heading text-fit">${oItem.Title}</h5>
                            <p class="list-group-item-text">${oItem.Descrip}</p>
                        </div>
                    </div>
                </div>`;
        });
              
        let oItemClose = `      </div>
                        </div>`;
        oItems += oItemOpen + oItemContent + oItemClose;
             
    });
    document.getElementById('menu-container').innerHTML = oItems;

  })
);

document.getElementById('btnVerMas').addEventListener('click',function(e){
    e.preventDefault();
    document.getElementById('menu-container').classList.toggle('hidden');
    document.getElementById('btn-hide_menu').classList.toggle('hidden');
    document.getElementById('menu-container').scrollIntoView();
});

document.getElementById('btn-hide_menu').addEventListener('click', function(e){
    e.preventDefault();
    document.getElementById('menu-container').classList.add('hidden');
    document.getElementById('marcas').scrollIntoView();
    document.getElementById('btn-hide_menu').classList.add('hidden');
});

const sendMail = 'mail/sendmail.php',
      formElements = document.querySelectorAll('input, textarea'),
      oFormData = {
            nombre: '',
            correo: '',
            telefono: '',
            mensaje: ''
      };

const stripTags = (input, allowed) => {
    allowed = (((allowed || '') + '')
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
        .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '')
        .replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });
}



/*
Author: Juan Donis
Description: The function check de Html5 Validity of the form input element
Args: {
    Array: Array to Iterate,
    Callback: Function to execute with the Array data
}
*/
const checkInputListener = elem => {

    let isValid;

    if (!elem.checkValidity()) {
        elem.parentElement.classList.remove('has-success');
        elem.parentElement.classList.add('has-error');
        elem.nextElementSibling.innerHTML = `<small>${elem.validationMessage}</small>`;
        isValid = false;
    } else {
        elem.parentElement.classList.remove('has-error');
        elem.parentElement.classList.add('has-success');
        elem.nextElementSibling.innerHTML = '<i class="fa fa-check-circle"></i>';
        isValid = true
    }
    return isValid;
}


Array.from(formElements, element => {
    element.addEventListener('keyup', e => checkInputListener(element));
    //element.addEventListener('blur', e => checkInputListener(element));
});




/*
Author: Juan Donis
Description: The script process the Contact Form and Fetch the data
Args: {
    Array: Array to Iterate,
    Callback: Function to execute with the Array data
}
*/

let btnSubmit = document.getElementById('btnEnviar'); //Button Selector

btnSubmit.addEventListener('click', e => {

    e.preventDefault(); //Prevent default behavior of Submit Button

    let invalidFields = new Array(); //Array to content the invalid fields

    Array.from(formElements, elem => {

        if (checkInputListener(elem)) {
            oFormData[elem.getAttribute('name')] = stripTags(elem.value, '<p><br/><br>'); //Add to FormData Object the element by input name : input value        
        } else {
            invalidFields.push(elem.getAttribute('name')); //If is not valid add the input name to the invalidFields Array       
        }
    });

    if (invalidFields.length == 0) { //if not exists invalid fields process the data with postJSON function     
        postJSON(sendMail, oFormData, res => {
            if (res.success) { //if response is success
                alertify.success(res.message) //show alert message with response
                let form = document.getElementById("ContactForm"); //assign form selector to local variable
                form.reset(); //reset the form

                Array.from(formElements, elem => { //for each form element
                    elem.nextElementSibling.innerHTML = '&nbsp;'; //reset the helper span text to white space
                });
            }
        })
    }
});

/*
Author: Juan Donis
Description: ASYNC FETCH VERSION - The script POST JSON data and Execute the especified callback function
Args{
    file: file path that will receive the data (php, api...),
    data: the data to send,
    callback: function to execute with the obtained data
}
*/
const postJSON = async (actionFile, data, callback) => {

    let oData = JSON.stringify(data);

    try {
        let oSettings = {
            method: 'POST',
            headers: new Headers(),
            body: oData
        }
        const response = await fetch(actionFile, oSettings)
        const result = await response.json()
        callback(result)
    } catch (err) {
        console.log(`Error: ${err.message}`)
    }
}
