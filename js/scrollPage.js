        var hash = 0;

        $('html, body').on('activate.bs.scrollspy', function () {
            
            hash = $(this).find("li.active a").attr("href").slice(1);
            //console.log(hash);              
           
        });

        $('.next').click(function(e){
            e.preventDefault();
            if(hash == 4){
                return;
            }else{
                hash++;  
            }
            
            if(hash == 3){
                $('html, body').scrollTo($('#'+hash) , 1250, { easing:'easeInOutExpo', offset:-50 }); 
            }else if(hash == 4){
                $('html, body').scrollTo($('#'+hash) , 1250, { easing:'easeInOutExpo', offset:100 });
            }
            else{
                $('html, body').scrollTo($('#'+hash) , 1250, { easing:'easeInOutExpo', offset:-20 });
            }

            console.log(hash);

             
            
        });
                                 
        $('.prev').click(function(e){
            e.preventDefault();
            if(hash == 1){
                $('html, body').scrollTo(0, 1250, { easing:'easeInOutExpo' });
            }
            if(hash > 0){             
                hash--;  
            }
                     
            if(hash == 3){
                $('html, body').scrollTo($('#'+hash) , 1250, { easing:'easeInOutExpo', offset:-50 }); 
            }else if(hash == 4){
                $('html, body').scrollTo($('#'+hash) , 1250, { easing:'easeInOutExpo', offset:100 });
            }else{
                $('html, body').scrollTo($('#'+hash) , 1250, { easing:'easeInOutExpo', offset:-20 });
            }
            console.log(hash);
        });